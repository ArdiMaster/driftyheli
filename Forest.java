import greenfoot.*;
import greenfoot.Greenfoot;

import java.lang.Integer;
import java.lang.Math;
import java.lang.Object;
import java.util.ArrayList;

/**
 * Write a description of class Forest here.
 * 
 * @author Adrian "ArdiMaster" Welcker
 * @version 2015-06-29
 */
public class Forest extends World
{
    int roundsPlayed = 0;
    int remainingDistance;
    int lastTreeMiddle = 300;

    final int maxDistance = 1950;
    final int minDistance = 350;

    /**
     * Constructor for objects of class Forest.
     * 
     */
    public Forest()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);
    }

    public void act() {
        roundsPlayed++;
        remainingDistance--;

        if ((roundsPlayed % 115) == 0) { // Every 115th round - when roundsPlayes / 115 leaves no remainder
            int treeMiddle = (Greenfoot.getRandomNumber(170) - 85 + lastTreeMiddle);

            if (treeMiddle < 80) { treeMiddle = 80; }
            if (treeMiddle > 520) { treeMiddle = 520; }

            this.addObject(new Tree(), treeMiddle - 90, 0);
            this.addObject(new Tree(), treeMiddle + 90, 0);
            lastTreeMiddle = treeMiddle;
        }

        if (remainingDistance == 0) {
            this.addObject(new Person(), (Greenfoot.getRandomNumber(170) - 85 + lastTreeMiddle), 0);
        }
    }

    public void started() {
        // for (Object obj : this.getObjects(Helicopter.class)) { }
        this.removeObjects(this.getObjects(Helicopter.class));
        this.removeObjects(this.getObjects(Tree.class));
        this.removeObjects(this.getObjects(Person.class));
        this.showText("", 300, 100);

        this.addObject(new Helicopter(), 300, 350);
        remainingDistance = (Greenfoot.getRandomNumber(maxDistance - minDistance) + minDistance);
    }
}
