import greenfoot.*;
import greenfoot.Greenfoot;

/**
 * Write a description of class Helicopter here.
 * 
 * @author Adrian "ArdiMaster" Welcker
 * @version 2015-06-29
 */
public class Helicopter extends Actor
{
    int moveLeft = 0;
    /**
     * Act - do whatever the Helicopter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if ((Greenfoot.isKeyDown("left") || Greenfoot.isKeyDown("space")) && moveLeft == 0) {
            moveLeft = 20;
        }

        if (moveLeft == 0) {
            this.setLocation(this.getX() + 1, this.getY());
        } else {
            this.setLocation(this.getX() - 2, this.getY());
            moveLeft--;
        }


        if (this.getOneIntersectingObject(Tree.class) != null) {
            this.getWorld().showText("Failure.", 300, 100);
            Greenfoot.stop();
        }

        if (this.getOneIntersectingObject(Person.class) != null) {
            this.getWorld().showText("Success!", 300, 100);
            Greenfoot.stop();
        }
    }    
}
