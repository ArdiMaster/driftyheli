import greenfoot.*;

/**
 * Write a description of class Tree here.
 * 
 * @author Adrian "ArdiMaster" Welcker
 * @version 2015-06-09
 */
public class Tree extends Actor
{
    /**
     * Act - do whatever the Tree wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        this.setLocation(this.getX(), this.getY() + 1);
        if (this.getY() >= 399) {
            this.getWorld().removeObject(this);
        }
    }    
}
