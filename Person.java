import greenfoot.*;

/**
 * Write a description of class Person here.
 * 
 * @author Adrian "ArdiMaster" Welcker
 * @version 2015-06-30
 */
public class Person extends Actor
{
    /**
     * Act - do whatever the Person wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        this.setLocation(this.getX(), this.getY() + 1);
        if (this.getY() >= 399) {
            this.getWorld().showText("Failure.", 300, 100);
            Greenfoot.stop();
        }
    }    
}
